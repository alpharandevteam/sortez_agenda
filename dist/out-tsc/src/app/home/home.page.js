import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
var HomePage = /** @class */ (function () {
    function HomePage(loadingController, nav, geolocation, nativeGeocoder) {
        this.loadingController = loadingController;
        this.nav = nav;
        this.geolocation = geolocation;
        this.nativeGeocoder = nativeGeocoder;
        this.x = 0;
        this.y = 0;
        this.code_postal = 0;
    }
    HomePage.prototype.detailspage = function () {
        this.nav.navigateForward('details');
    };
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            // resp.coords.latitude
            // resp.coords.longitude
            _this.x = resp.coords.longitude;
            _this.y = resp.coords.latitude;
            // alert("x: "+this.x)
            // alert("y: "+this.y)
            // console.log("x arrondi: "+parseFloat(this.x.toFixed(3)))
            // console.log("y arrondi: "+parseFloat(this.y.toFixed(3)))
            _this.nativeGeocoder.reverseGeocode(45.8333, 5.6, { useLocale: true, maxResults: 1 }).then(function (result) {
                var data = result;
                alert(data[0].postalCode);
            }).catch(function (error) { return alert(error); });
        }).catch(function (error) {
            alert('Error getting location' + error);
        });
        // this.nativeGeocoder.reverseGeocode(this.y, this.x, { useLocale: true, maxResults: 1 }).then(
        //   (result: NativeGeocoderResult[]) => {
        //     alert(JSON.stringify(result[0]))
        //   }
        // ).catch((error: any) => alert(error));
        // this.nativeGeocoder.reverseGeocode(this.y, this.x, { useLocale: true, maxResults: 1 }).then((result: NativeGeocoderResult[]) => {
        //   var data = result[0]
        //   this.code_postal = parseInt(data.postalCode)
        // }).catch((error: any) => console.log(error));
        // this.nativeGeocoder.reverseGeocode(this.y, this.x, { useLocale: true, maxResults: 1 }).then(
        //   (result: NativeGeocoderResult[]) => alert(JSON.stringify(result[0]))
        // ).catch((error: any) => alert(error));
    };
    HomePage = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: 'home.page.html',
            styleUrls: ['home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [LoadingController, NavController, Geolocation, NativeGeocoder])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map