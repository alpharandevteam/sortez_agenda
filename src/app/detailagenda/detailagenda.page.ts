import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { NavController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-detailagenda',
  templateUrl: './detailagenda.page.html',
  styleUrls: ['./detailagenda.page.scss'],
})
export class DetailagendaPage implements OnInit {
  id_agenda;
  data;
  constructor(private route: ActivatedRoute,private http: HTTP,private nav:NavController,private loadingcontroller:LoadingController) {
    
  }

  ngOnInit() {
    this.id_agenda = this.route.snapshot.params['id'];
    //alert(this.id_agenda);
    let postData = { idAgenda: this.id_agenda };
  //  //alert("post liste angenda");
  //   //alert(JSON.stringify(postData));
    var headers = { "Content-Type": 'application/json' };
      this.http.post("https://www.sortez.org/sortez_pro/sortez_pro_mobile/get_agendaMobile_byId/", postData, headers)
      .then(data => {
        //alert('Donnée récuperer');
        this.data= JSON.parse(data.data);
       //alert(JSON.stringify(this.data));
      }, error => {
        alert('Pas de donnée');
        console.log(error);
      });   
  }

}
