import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailagendaPage } from './detailagenda.page';

describe('DetailagendaPage', () => {
  let component: DetailagendaPage;
  let fixture: ComponentFixture<DetailagendaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailagendaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailagendaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
