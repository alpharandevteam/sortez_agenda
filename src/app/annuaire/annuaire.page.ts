import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-annuaire',
  templateUrl: './annuaire.page.html',
  styleUrls: ['./annuaire.page.scss'],
})
export class AnnuairePage implements OnInit {
  data;
  code_Postale;
  constructor(private http: HTTP,private route: ActivatedRoute,private nav:NavController, private load:LoadingController) { 
    
  }

  ngOnInit() {
    // this.code_Postale = this.route.snapshot.params['codePostal'];
    // //alert(this.code_Postale);
     let postData = { idville: '06000' };
    // let postData = { codePostal: this.code_Postale };
    var headers = { "Content-Type": 'application/json' };
    this.http.post("https://www.sortez.org/sortez_pro/sortez_pro_mobile/get_list_annuaireMobile/", postData, headers)
      .then(data => {
        //alert('Donnée annuaire récuperer');
        this.data= JSON.parse(data.data);
      //alert(JSON.stringify(this.data));
      }, error => {
        alert('Pas de donnée');
        console.log(error);
      });
  }
  gotodetails(id){
    this.nav.navigateForward('details-annuaire/' + id);
  }

}
