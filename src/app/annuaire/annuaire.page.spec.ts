import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnuairePage } from './annuaire.page';

describe('AnnuairePage', () => {
  let component: AnnuairePage;
  let fixture: ComponentFixture<AnnuairePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnuairePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnuairePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
