import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-listeangenda',
  templateUrl: './listeangenda.page.html',
  styleUrls: ['./listeangenda.page.scss'],
})
export class ListeangendaPage implements OnInit {

  code_Postale;
  data;
  constructor(private route: ActivatedRoute,private http: HTTP,private nav:NavController,private loadingcontroller:LoadingController) {

  }

  ngOnInit() {
    this.code_Postale = this.route.snapshot.params['codePostal'];
    //alert("liste agenda");
    //alert(this.code_Postale);
    let postData = { codePostal: this.code_Postale };
  //  //alert("post liste angenda");
  //   //alert(JSON.stringify(postData));
    var headers = { "Content-Type": 'application/json' };
      this.http.post("https://www.sortez.org/sortez_pro/sortez_pro_mobile/get_all_list_agendaMobile/", postData, headers)
      .then(data => {
        //alert('Donnée récuperer');
        this.data= JSON.parse(data.data);
       //alert(JSON.stringify(this.data));
      }, error => {
        alert('Pas de donnée');
        console.log(error);
      });   
  }

  detail_agenda(id){
    this.nav.navigateForward('detailagenda/'+ id);
  }

}
