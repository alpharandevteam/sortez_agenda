import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeangendaPage } from './listeangenda.page';

describe('ListeangendaPage', () => {
  let component: ListeangendaPage;
  let fixture: ComponentFixture<ListeangendaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeangendaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeangendaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
