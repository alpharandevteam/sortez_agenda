import { Component, OnInit, ViewChild  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { NavController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-listannonce',
  templateUrl: './listannonce.page.html',
  styleUrls: ['./listannonce.page.scss'],
})
export class ListannoncePage implements OnInit {
  annonceId;
  data;
  code_Postale;
  constructor(private nav:NavController,private route: ActivatedRoute,private http: HTTP,private loadingController: LoadingController) { }
  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Chargement ...',
    });
    await loading.present();
   // this.annonceId = this.route.snapshot.params['id'];
   //this.annonceId = '06000';
   this.code_Postale = this.route.snapshot.params['codePostal'];
   let postData = { annonceId: this.code_Postale };
    //alert(JSON.stringify(postData));
    var headers = { "Content-Type": 'application/json' };
      this.http.post("https://www.sortez.org/sortez_pro/sortez_pro_mobile/get_all_annonce_by_cod_postal/", postData, headers)
      .then(async data => {
        this.data= JSON.parse(data.data);
        await loading.dismiss();
      }, error => {
        alert('Pas de donnée');
        console.log(error);
      });
  }
  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.data.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }
  detail_annonce(id){
    this.annonceId = '06000';
   let postData = { annonceId: id };
    //alert(JSON.stringify(postData));
    var headers = { "Content-Type": 'application/json' };
      this.http.post("https://www.sortez.org/sortez_pro/sortez_pro_mobile/get_by_id_annonce_by_cod_postal/", postData, headers)
      .then(data => {
        //alert('Donnée récuperer');
        let detail= JSON.parse(data.data);
       //alert(JSON.stringify(detail));
       this.nav.navigateForward('detailannonce/' + id);
      }, error => {
        alert('Pas de donnée');
        console.log(error);
      });
  } 
}