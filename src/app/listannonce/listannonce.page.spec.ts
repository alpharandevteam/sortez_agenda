import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListannoncePage } from './listannonce.page';

describe('ListannoncePage', () => {
  let component: ListannoncePage;
  let fixture: ComponentFixture<ListannoncePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListannoncePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListannoncePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
