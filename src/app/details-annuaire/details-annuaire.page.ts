import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-details-annuaire',
  templateUrl: './details-annuaire.page.html',
  styleUrls: ['./details-annuaire.page.scss'],
})
export class DetailsAnnuairePage implements OnInit {
  idcommercant;
  data;
  constructor(private http: HTTP,private route: ActivatedRoute) { 
    this.idcommercant = this.route.snapshot.params['id'];
  }

  ngOnInit() {
    let postData = { IdCommercant: this.idcommercant };

    var headers = { "Content-Type": 'application/json' };
    this.http.post("https://www.sortez.org/sortez_pro/sortez_pro_mobile/get_annuaire_by_id/", postData, headers)
      .then(data => {
        //alert('Donnée récuperer');
        this.data= JSON.parse(data.data);
      //alert(JSON.stringify(this.data));
      }, error => {
        alert('Pas de donnée');
        console.log(error);
      });
  }

}
