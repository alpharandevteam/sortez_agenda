import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsAnnuairePage } from './details-annuaire.page';

describe('DetailsAnnuairePage', () => {
  let component: DetailsAnnuairePage;
  let fixture: ComponentFixture<DetailsAnnuairePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsAnnuairePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsAnnuairePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
