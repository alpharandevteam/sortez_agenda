import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetailsAnnuairePage } from './details-annuaire.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsAnnuairePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetailsAnnuairePage]
})
export class DetailsAnnuairePageModule {}
