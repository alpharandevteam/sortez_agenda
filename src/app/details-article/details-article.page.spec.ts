import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsArticlePage } from './details-article.page';

describe('DetailsArticlePage', () => {
  let component: DetailsArticlePage;
  let fixture: ComponentFixture<DetailsArticlePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsArticlePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsArticlePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
