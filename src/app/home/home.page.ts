import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NavController, LoadingController } from '@ionic/angular';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { HTTP } from '@ionic-native/http/ngx';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  geoLatitude: number;
  geoLongitude: number;
  geoAccuracy:number;
  geoAddress: string;
 
  watchLocationUpdates:any; 
  loading:any;
  isWatching:boolean;
  data;
  codePostal;
  //Geocoder configuration
  geoencoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 1
  };
  constructor(
    private geolocation: Geolocation,
    private http: HTTP,
    private nativeGeocoder: NativeGeocoder, private nav:NavController, private loadingcontroller:LoadingController
  ) {
    //alert("test");
    this.geolocation.getCurrentPosition().then((resp) => {
      this.geoLatitude = resp.coords.latitude;
      this.geoLongitude = resp.coords.longitude;       
      //alert("geolocalisation");
      // this.geoLatitude = 43.7;
      // this.geoLongitude = 7.25; 
      this.geoAccuracy = resp.coords.accuracy; 
      this.getGeoencoder(this.geoLatitude,this.geoLongitude);
     }).catch((error) => {
       //alert('Error getting location'+ JSON.stringify(error));
     });
  }
 
  
    // Get current coordinates of device
    getGeolocation(){
      this.geolocation.getCurrentPosition().then((resp) => {
        // this.geoLatitude = resp.coords.latitude;
        // this.geoLongitude = resp.coords.longitude; 
        this.geoLatitude = 43.7;
        this.geoLongitude = 7.25; 
        this.geoAccuracy = resp.coords.accuracy; 
        this.getGeoencoder(this.geoLatitude,this.geoLongitude);
       }).catch((error) => {
         //alert('Error getting location'+ JSON.stringify(error));
       });
    }
  
    //geocoder method to fetch address from coordinates passed as arguments
    async getGeoencoder(latitude,longitude){
      // this.nativeGeocoder.reverseGeocode(latitude, longitude, this.geoencoderOptions)
      this.nativeGeocoder.reverseGeocode(latitude, longitude, this.geoencoderOptions)
      .then(async (result: NativeGeocoderResult[]) => {

        // this.geoAddress = this.generateAddress(result[0]);        
        const loading = await this.loadingcontroller.create({
          message: 'Chargement ...',
        });
        await loading.present();
        //alert(this.codePostal); 
        this.codePostal="06000";
        //alert("code postal");
        
        //script mams     
      let postData = { idville: "06000" };
      // //alert("data post data");            
      var headers = { "Content-Type": 'application/json' };
      this.http.post("https://www.sortez.org/sortez_pro/sortez_pro_mobile/get_list_agendaMobile/", postData, headers)
      .then(data => {
        //alert('Donnée récuperer');
        this.data= JSON.parse(data.data);
        //alert(JSON.stringify(this.data));
        loading.dismiss();
      }, error => {
        alert('Pas de donnée');
        //alert(JSON.stringify(error));
        console.log(error);
        loading.dismiss();
      });
      })
      .catch((error: any) => {
        //alert('Error getting location'+ JSON.stringify(error));
      });
    }
  
    //Return Comma saperated address
    generateAddress(addressObj){
        let obj = [];
        let address = "";
        for (let key in addressObj) {
          obj.push(addressObj[key]);
        }
        obj.reverse();
        for (let val in obj) {
          if(obj[val].length)
          address += obj[val]+', ';
        }
      return address.slice(0, -2);
    }
  
  
    //Start location update watch
    watchLocation(){
      this.isWatching = true;
      this.watchLocationUpdates = this.geolocation.watchPosition();
      this.watchLocationUpdates.subscribe((resp) => {
        // this.geoLatitude = resp.coords.latitude;
        // this.geoLongitude = resp.coords.longitude; 
        this.geoLatitude = 43.7;
        this.geoLongitude = 7.25; 
        this.getGeoencoder(this.geoLatitude,this.geoLongitude);
      });
    }
  
    //Stop location update watch
    stopLocationWatch(){
      this.isWatching = false;
      this.watchLocationUpdates.unsubscribe();
    }
    gotodetails(id){
      this.nav.navigateForward('details/' + id);
    }

    voirPlusAnnuaire(codePostal){
      //alert("clik annuaire");
      //alert(codePostal);
      this.nav.navigateForward('annuaire/' + codePostal);
    }
    voirPlusAnnonce(codePostal){
      //alert("clik annonce");
      //alert(codePostal);
      this.nav.navigateForward('listannonce/' + codePostal);
    }
    voirPlusAgenda(codePostal){
      //alert("clik angenda");
      //alert(codePostal);
      this.nav.navigateForward('listeangenda/' + codePostal);
    }
    voirPlusArticle(codePostal){
      //alert("clik article");
      //alert(codePostal);
      this.nav.navigateForward('article/' + codePostal);
    }
}
