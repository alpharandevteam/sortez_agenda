import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'annuaire', loadChildren: './annuaire/annuaire.module#AnnuairePageModule' },
  { path: 'listannonce/:codePostal', loadChildren: './listannonce/listannonce.module#ListannoncePageModule' },
  { path: 'listeangenda/:codePostal', loadChildren: './listeangenda/listeangenda.module#ListeangendaPageModule' },
  { path: 'article', loadChildren: './article/article.module#ArticlePageModule' },
  { path: 'detailannonce/:id', loadChildren: './detailannonce/detailannonce.module#DetailannoncePageModule' },
  { path: 'detailagenda/:id', loadChildren: './detailagenda/detailagenda.module#DetailagendaPageModule' },
  { path: 'details-annuaire/:id', loadChildren: './details-annuaire/details-annuaire.module#DetailsAnnuairePageModule' },
  { path: 'details-article/:id', loadChildren: './details-article/details-article.module#DetailsArticlePageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
