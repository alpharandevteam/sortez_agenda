import { Component, OnInit, ViewChild  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { NavController,IonInfiniteScroll,LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-detailannonce',
  templateUrl: './detailannonce.page.html',
  styleUrls: ['./detailannonce.page.scss'],
})
export class DetailannoncePage implements OnInit {
  detail;
  constructor(private nav:NavController,private route: ActivatedRoute,private http: HTTP,public loadingController: LoadingController) { }

 async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Chargement ...',
    });
    await loading.present();
   let id  = this.route.snapshot.params['id'];
   let postData = { annonceId: id };
    //alert(JSON.stringify(postData));
    var headers = { "Content-Type": 'application/json' };
      this.http.post("https://www.sortez.org/sortez_pro/sortez_pro_mobile/get_by_id_annonce_by_cod_postal/", postData, headers)
      .then(async data => {
        let detail= JSON.parse(data.data);
        await loading.dismiss();
       console.log(detail);
       detail = 
      this.detail = detail;
      }, error => {
        alert('Pas de donnée');
        console.log(error);
      });
  }
  
}
