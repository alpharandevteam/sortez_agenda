import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailannoncePage } from './detailannonce.page';

describe('DetailannoncePage', () => {
  let component: DetailannoncePage;
  let fixture: ComponentFixture<DetailannoncePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailannoncePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailannoncePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
