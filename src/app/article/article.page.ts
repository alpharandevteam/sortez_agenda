import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { NavController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-article',
  templateUrl: './article.page.html',
  styleUrls: ['./article.page.scss'],
})
export class ArticlePage implements OnInit {
  code_Postale;
  data;
  constructor(private route: ActivatedRoute,private http: HTTP,private nav:NavController) { }

  ngOnInit() {
    // this.code_Postale = this.route.snapshot.params['codePostal'];
    // //alert("code posta article");
    // //alert(this.code_Postale);
    // let postData = { codePostal: this.code_Postale };
    let postData = { idville: '06000' };
    var headers = { "Content-Type": 'application/json' };
    this.http.post("https://www.sortez.org/sortez_pro/sortez_pro_mobile/get_list_articleMobile/", postData, headers)
      .then(data => {
        //alert('Donnée article récuperer');
        this.data= JSON.parse(data.data);
      //alert(JSON.stringify(this.data));
      }, error => {
        alert('Pas de donnée');
        console.log(error);
      });
  }
  gotodetails(id){
    this.nav.navigateForward('details-article/' + id);
  }
}
